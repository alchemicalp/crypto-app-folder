import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CryptoDataService } from '../../services/crypto-data.service';

@Component({
  selector: 'app-crypto-ranking',
  templateUrl: './crypto-ranking.component.html'
})
export class CryptoRankingComponent implements OnInit {

  public data = [];
  public searchText : string;

  constructor(private _CryptoDataService: CryptoDataService, private _router: Router) { }

  onSelect(currency) {
   this._router.navigate(['/crypto-display', currency.id]);
  }

  ngOnInit() {
    this._CryptoDataService.getCryptoData()
      .subscribe(receivedData => this.data = receivedData);
  }

}
