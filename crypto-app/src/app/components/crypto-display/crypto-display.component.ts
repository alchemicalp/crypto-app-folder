import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { CryptoDataService } from '../../services/crypto-data.service';

@Component({
  selector: 'app-crypto-display',
  templateUrl: './crypto-display.component.html'
})
export class CryptoDisplayComponent implements OnInit {

  public data = [];
  public currencyId;
  public currency = [];

  constructor(private _CryptoDataService: CryptoDataService, private _router: Router, private _route: ActivatedRoute) { }

  onClick() {
   this._router.navigate(['/']);
  }

  ngOnInit() {
    this._CryptoDataService.getCryptoData()
    .subscribe(receivedData =>  {
        this.data = receivedData;
        let x = this._route.snapshot.paramMap.get('id');
        this.currencyId = x;
        let result = this.data.find((c) => c.id === this.currencyId);
        this.currency = result;
      }
    );
  }
}
