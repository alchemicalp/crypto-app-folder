import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CryptoRankingComponent } from '././components/crypto-ranking/crypto-ranking.component';
import { CryptoDisplayComponent } from '././components/crypto-display/crypto-display.component';

const routes: Routes = [
  {path: '', component: CryptoRankingComponent },
  {path: 'crypto-display/:id', component: CryptoDisplayComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [CryptoRankingComponent, CryptoDisplayComponent]
