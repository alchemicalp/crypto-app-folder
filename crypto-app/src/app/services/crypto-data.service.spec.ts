import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { CryptoDataService } from './crypto-data.service';

describe('CryptoDataService', () => {
  let service: CryptoDataService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CryptoDataService]
    });

    service = TestBed.get(CryptoDataService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
  httpMock.verify();
});

  it('should retrieve data from the API via GET', () => {
    const dummyData: Data[] = [
      {
        id: 'bitcoin',
        name: 'Bitcoin',
        symbol: 'BTC',
        rank: '1',
        price_usd: '235.21561',
        price_btc: '6559.6519',
        "24h_volume_usd": '65165916516.5',
        market_cap_usd: '65165195165.76',
        available_supply: '576759.76',
        total_supply: '60659.67',
        max_supply: '87598.951',
        percent_change_1h: '6757.516',
        percent_change_24h: '6166.25',
        percent_change_7d: '191.7589',
        last_updated: '12345678910',
      },
      {
        id: 'litecoin',
        name: 'Litecoin',
        symbol: 'LTC',
        rank: '13',
        price_usd: '235.21561',
        price_btc: '6559.6519',
        "24h_volume_usd": '65165916516.5' ,
        market_cap_usd: '65165195165.76',
        available_supply: '576759.76' ,
        total_supply: '60659.67',
        max_supply: '87598.951',
        percent_change_1h: '6757.516',
        percent_change_24h: '6166.25',
        percent_change_7d: '191.7589',
        last_updated: '12345678910',
      },
    ];

    service.getCryptoData().subscribe(data => {
      expect(Data.length)toBe(2);
      expect(Data).toBe(dummyData);
    });

    const request = httpMock.expectOne('https://api.coinmarketcap.com/v1/ticker/');

    expect(request.request.method).toBe('GET');

    request.flush(dummyData);
  });
});
