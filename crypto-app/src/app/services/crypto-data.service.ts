import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Data } from '../models/data.model';

@Injectable({
  providedIn: 'root'
})
export class CryptoDataService {

  public _url: string = "https://api.coinmarketcap.com/v1/ticker/";

  constructor(public _http: HttpClient) { }

  getCryptoData() {
    return this._http.get<Data[]>(this._url);
  }

}
